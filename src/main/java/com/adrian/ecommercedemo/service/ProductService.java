package com.adrian.ecommercedemo.service;

import com.adrian.ecommercedemo.domain.Product;
import com.adrian.ecommercedemo.util.Filter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {

  Product findById(Long id);

  Page<Product> findAll(Filter filter, Pageable pageable);

  Product save(Product product);

  void deleteById(Long id);

  Product update(Long id, Product product);

}
