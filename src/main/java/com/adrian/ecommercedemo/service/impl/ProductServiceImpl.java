package com.adrian.ecommercedemo.service.impl;

import com.adrian.ecommercedemo.domain.Product;
import com.adrian.ecommercedemo.util.Filter;
import com.adrian.ecommercedemo.mappers.ProductMapper;
import com.adrian.ecommercedemo.repository.ProductRepository;
import com.adrian.ecommercedemo.service.ProductService;
import com.adrian.ecommercedemo.specifications.ProductSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;
  private final ProductMapper productMapper;

  @Override
  public Product findById(Long id) {
    return productRepository.findById(id).get();
  }

  @Override
  public Page<Product> findAll(Filter filter, Pageable pageable) {
    ProductSpecification productSpecification = new ProductSpecification(filter);
    return productRepository.findAll(productSpecification, pageable);
  }

  @Override
  public Product save(Product product) {
    product.setCreatedAt(LocalDate.now());
    return productRepository.save(product);
  }

  @Override
  public void deleteById(Long id) {
    productRepository.deleteById(id);
  }

  @Override
  public Product update(Long id, Product product) {
    Product productToUpdate = this.findById(id);
    productMapper.update(productToUpdate, product);
    return productRepository.save(productToUpdate);
  }
}
