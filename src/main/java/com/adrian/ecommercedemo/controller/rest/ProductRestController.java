package com.adrian.ecommercedemo.controller.rest;

import com.adrian.ecommercedemo.domain.Product;
import com.adrian.ecommercedemo.util.Filter;
import com.adrian.ecommercedemo.dto.ProductDto;
import com.adrian.ecommercedemo.mappers.ProductDtoMapper;
import com.adrian.ecommercedemo.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductRestController {

  private final ProductService productService;
  private final ProductDtoMapper productDtoMapper;

  @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
  @ResponseStatus(value = HttpStatus.OK)
  public Page<Product> findAll(Filter filter, Pageable pageable) {
    log.debug("[findAll] filters: {}, pageable: {}", filter.toString(), pageable.toString());
    return productService.findAll(filter, pageable);
  }

  @GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
  @ResponseStatus(value = HttpStatus.OK)
  public Product findById(@PathVariable Long id) {
    log.debug("[findById] id: {}", id);
    return productService.findById(id);
  }

  @PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
  @ResponseStatus(value = HttpStatus.CREATED)
  public Product save(@RequestBody @Valid ProductDto productDto) {
    log.debug("[save] productDto: {}", productDto.toString());
    Product product = productDtoMapper.productToProductSave(productDto);
    return productService.save(product);
  }

  @DeleteMapping(path = "/{id}")
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void deleteById(@PathVariable Long id) {
    log.debug("[deleteById] id: {}", id);
    productService.deleteById(id);
  }

  @PatchMapping(path = "/{id}")
  @ResponseStatus(value = HttpStatus.OK)
  public void update(@PathVariable Long id, @RequestBody Product product) {
    log.debug("[update] id: {}, productUpdate: {}", id, product.toString());
    productService.update(id, product);
  }
}
