package com.adrian.ecommercedemo.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class ControllerAdvice {


  @ExceptionHandler({NoSuchElementException.class})
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @RequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
  public NoSuchElementException noSuchElementExceptionHandler(NoSuchElementException exception) {
    return exception;
  }
}
