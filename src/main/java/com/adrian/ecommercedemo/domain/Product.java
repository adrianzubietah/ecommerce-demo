package com.adrian.ecommercedemo.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String code;
  private String line;
  private String maker;
  private String position;
  private String description;
  private Integer stock;
  private BigDecimal price;
  private LocalDate createdAt;

}
