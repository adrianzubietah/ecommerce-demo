package com.adrian.ecommercedemo.mappers;

import com.adrian.ecommercedemo.domain.Product;
import com.adrian.ecommercedemo.dto.ProductDto;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface ProductDtoMapper {

  @Mapping(source = "productDto", target = "description", qualifiedByName = "getDescription")
  @Mapping(source = "productDto.line", target = "line", qualifiedByName = "getCapitalize")
  @Mapping(source = "productDto.maker", target = "maker", qualifiedByName = "getCapitalize")
  @Mapping(source = "productDto.position", target = "position", qualifiedByName = "getCapitalize")
  Product productToProductSave(ProductDto productDto);

  @Named("getDescription")
  static String getDescription(ProductDto productDto) {
    return new StringBuilder(StringUtils.capitalize(productDto.getLine()))
        .append(" ")
        .append(StringUtils.capitalize(productDto.getPosition()))
        .append(" ")
        .append(productDto.getMaker().toUpperCase())
        .toString();
  }

  @Named("getCapitalize")
  static String getCapitalize(String text) {
    return StringUtils.capitalize(text);
  }

}
