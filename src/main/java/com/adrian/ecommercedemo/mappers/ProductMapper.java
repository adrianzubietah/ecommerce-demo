package com.adrian.ecommercedemo.mappers;

import com.adrian.ecommercedemo.domain.Product;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface ProductMapper {

  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  void update(@MappingTarget Product productToUpdate, Product product);

}
