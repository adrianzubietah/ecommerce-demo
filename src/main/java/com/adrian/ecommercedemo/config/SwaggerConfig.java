package com.adrian.ecommercedemo.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class SwaggerConfig {

  @Bean
  public OpenAPI customOpenAPI() {
    OpenAPI openAPI =  new OpenAPI()
        .info(new Info()
            .title("Products")
            .version("version"));
    return openAPI;
  }

}
