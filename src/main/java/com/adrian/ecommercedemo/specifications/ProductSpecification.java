package com.adrian.ecommercedemo.specifications;

import com.adrian.ecommercedemo.domain.Product;
import com.adrian.ecommercedemo.util.Filter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ProductSpecification implements Specification<Product> {

  private static final char CHARACTER_LIKE = '%';
  private final Filter filter;

  @Override
  public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    List<Predicate> predicates = new ArrayList<>();

    if (this.verifyFilterAndValueValid()) {
      Predicate predicate = builder
          .like(root.get(filter.getKey()), CHARACTER_LIKE + filter.getValue() + CHARACTER_LIKE);
      predicates.add(predicate);
    }

    query.distinct(true);
    if (predicates.isEmpty()) {
      return null;
    } else {
      return predicates.stream().reduce(builder::and).get();
    }
  }

  private boolean verifyFilterAndValueValid() {
    return filter != null && !filter.getKey().isEmpty() && !filter.getValue().isEmpty();
  }
}
