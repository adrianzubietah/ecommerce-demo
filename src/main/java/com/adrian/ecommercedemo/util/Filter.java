package com.adrian.ecommercedemo.util;

import lombok.Data;

@Data
public class Filter {
  
  private String key = "";
  private String value = "";

}
