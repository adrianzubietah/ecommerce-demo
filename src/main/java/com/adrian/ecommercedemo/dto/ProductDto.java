package com.adrian.ecommercedemo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ProductDto {

  @NotNull
  private String code;
  @NotNull
  private String line;
  @NotNull
  private String maker;
  @NotNull
  private String position;
  @NotNull
  private Integer stock;
  @NotNull
  private BigDecimal price;

}
