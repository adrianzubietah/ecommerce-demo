package com.adrian.ecommercedemo;

import com.adrian.ecommercedemo.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import java.math.BigDecimal;


@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureMockRestServiceServer
public abstract class EcommerceDemoApplicationTests {

  @Autowired
  protected MockMvc mockMvc;

  @Autowired
  protected EntityManager entityManager;

  @Autowired
  protected JdbcTemplate jdbcTemplate;

  protected Product generateProductInDB(String name) {
    Product product = new Product();
    product.setCode("code");
    product.setLine(name);
    product.setPosition("Derecha");
    product.setMaker("Atma");
    product.setDescription("description");
    product.setStock(2);
    product.setPrice(new BigDecimal("100.00"));

    entityManager.persist(product);
    entityManager.flush();
    return product;
  }

}
