package com.adrian.ecommercedemo.service;

import com.adrian.ecommercedemo.EcommerceDemoApplicationTests;
import com.adrian.ecommercedemo.domain.Product;
import com.adrian.ecommercedemo.util.Filter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
public class ProductServiceTest extends EcommerceDemoApplicationTests {

  @Autowired
  private ProductService productService;

  @Test
  public void findById_withIdExisting_returnIdExisting() {
    Product productSaved = this.generateProductInDB("optica");

    Product product = productService.findById(productSaved.getId());

    assertEquals(productSaved.getId(), product.getId());
  }

  @Test
  public void findById_withIdNonExisting_throwNonSuchElementException() {
    assertThrows(NoSuchElementException.class, () -> productService.findById(999L));
  }

  @Test
  public void findAll_withoutFilter_returnAllProducts() {
    this.generateSixProductInDB();

    Page<Product> products = productService.findAll(null, PageRequest.of(0, 10));

    assertEquals(6L, products.getContent().size());
  }

  @Test
  public void findAll_withFilterName_returnPageWithSizeThree() {
    this.generateSixProductInDB();

    Filter filter = new Filter();
    filter.setKey("line");
    filter.setValue("Capot");

    Page<Product> products = productService.findAll(filter, PageRequest.of(0, 10));

    assertEquals(1L, products.getContent().size());
    assertEquals(filter.getValue(), products.getContent().get(0).getLine());
  }

  @Test
  public void save_withFieldsValid_returnProductSaved() {
    Product productToSave = new Product();
    productToSave.setLine("aleron");
    productToSave.setCode("00001");
    productToSave.setPrice(new BigDecimal("100.00"));
    productToSave.setMaker("vic");
    productToSave.setStock(4);
    productToSave.setPosition("derecha");

    Product productSaved = productService.save(productToSave);

    assertNotNull(productSaved.getId());
    assertNotNull(productSaved.getCreatedAt());
  }

  @Test
  public void deleteById_withIdExisting_deletedProduct() {
    Product productSaved = this.generateProductInDB("optica");

    int rowCountBefore = JdbcTestUtils.countRowsInTable(jdbcTemplate, "product");

    productService.deleteById(productSaved.getId());
    entityManager.flush();

    int rowCountAfter = JdbcTestUtils.countRowsInTable(jdbcTemplate, "product");

    assertEquals(rowCountBefore - 1, rowCountAfter);

  }

  @Test
  public void update_withIdExisting_updatedProduct() {
    Product productSaved = this.generateProductInDB("optica");

    Product productUpdateDto = new Product();
    productUpdateDto.setPrice(new BigDecimal("20000.10"));

    Product productUpdated = productService.update(productSaved.getId(), productUpdateDto);
    entityManager.flush();

    assertEquals(productSaved.getId(), productUpdated.getId());
    assertEquals(productSaved.getCode(), productUpdated.getCode());
    assertEquals(productSaved.getPrice(), productUpdateDto.getPrice());
  }

  @Test
  public void update_withIdExisting_throwNoSuchElementException() {
    assertThrows(NoSuchElementException.class, () -> productService.update(999L, new Product()));
  }

  private void generateSixProductInDB() {
    this.generateProductInDB("Optica");
    this.generateProductInDB("Capot");
    this.generateProductInDB("Paragolpe");
    this.generateProductInDB("Baliza");
    this.generateProductInDB("Faro");
    this.generateProductInDB("Reten de Optica");
  }

}
