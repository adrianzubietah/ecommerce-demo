package com.adrian.ecommercedemo.controller;

import com.adrian.ecommercedemo.EcommerceDemoApplicationTests;
import com.adrian.ecommercedemo.domain.Product;
import com.adrian.ecommercedemo.dto.ProductDto;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
public class ProductRestControllerTest extends EcommerceDemoApplicationTests {

  @Test
  @SneakyThrows
  public void findAll_withFieldsValid_returnAllProduct() {
    mockMvc
        .perform(get("/products"))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andReturn();
  }

  @Test
  @SneakyThrows
  public void findById_withIdExisting_returnOneProduct() {
    Product product = this.generateProductInDB("Optica");

    mockMvc
        .perform(get("/products/" + product.getId()))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andReturn();
  }

  @Test
  @SneakyThrows
  public void findById_withIdNonExisting_throwNonSuchElementException() {
    mockMvc
        .perform(get("/products/999"))
        .andExpect(status().isNotFound());
  }

  @Test
  @SneakyThrows
  public void save_withFieldsValid_createProduct() {
    ProductDto productDto = this.generateProductDtoDefault();

    Gson gson = new Gson();
    mockMvc
        .perform(post("/products")
          .content(gson.toJson(productDto))
          .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isCreated())
        .andReturn();
  }

  @Test
  @SneakyThrows
  public void save_withMakerNull_throwsBadRequest() {
    ProductDto productDto = this.generateProductDtoDefault();
    productDto.setMaker(null);

    Gson gson = new Gson();
    mockMvc
        .perform(post("/products")
            .content(gson.toJson(productDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void save_withPositionNull_throwsBadRequest() {
    ProductDto productDto = this.generateProductDtoDefault();
    productDto.setPosition(null);

    Gson gson = new Gson();
    mockMvc
        .perform(post("/products")
            .content(gson.toJson(productDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void save_withCodeNull_throwsBadRequest() {
    ProductDto productDto = this.generateProductDtoDefault();
    productDto.setCode(null);

    Gson gson = new Gson();
    mockMvc
        .perform(post("/products")
            .content(gson.toJson(productDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void save_withLineNull_throwsBadRequest() {
    ProductDto productDto = this.generateProductDtoDefault();
    productDto.setLine(null);

    Gson gson = new Gson();
    mockMvc
        .perform(post("/products")
            .content(gson.toJson(productDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void save_withStockNull_throwsBadRequest() {
    ProductDto productDto = this.generateProductDtoDefault();
    productDto.setStock(null);

    Gson gson = new Gson();
    mockMvc
        .perform(post("/products")
            .content(gson.toJson(productDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void save_withPriceNull_throwsBadRequest() {
    ProductDto productDto = this.generateProductDtoDefault();
    productDto.setPrice(null);

    Gson gson = new Gson();
    mockMvc
        .perform(post("/products")
            .content(gson.toJson(productDto))
            .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isBadRequest());
  }

  @Test
  @SneakyThrows
  public void deleteById_withIdExisting_returnVoid() {
    Product product = this.generateProductInDB("Optica");

    mockMvc
        .perform(delete("/products/" + product.getId()))
        .andExpect(status().isNoContent())
        .andReturn();
  }

  private ProductDto generateProductDtoDefault() {
    ProductDto productDto = new ProductDto();
    productDto.setCode("00001");
    productDto.setLine("line");
    productDto.setMaker("maker");
    productDto.setPosition("position");
    productDto.setStock(2);
    productDto.setPrice(new BigDecimal("100.00"));
    return productDto;
  }

}
