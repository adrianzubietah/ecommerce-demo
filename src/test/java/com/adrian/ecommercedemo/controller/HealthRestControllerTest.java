package com.adrian.ecommercedemo.controller;

import com.adrian.ecommercedemo.EcommerceDemoApplicationTests;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HealthRestControllerTest extends EcommerceDemoApplicationTests {

  @Test
  @SneakyThrows
  public void healthCheck_withAllModulesOkUsingHttp_returnStatusOk() {
    mockMvc
        .perform(get("/actuator/health"))
        .andExpect(status().isOk())
        .andExpect(content().contentType("application/vnd.spring-boot.actuator.v3+json"))
        .andReturn();
  }

}
